//
//  AppDelegate.swift
//  AVRControl
//
//  Created by Daniel Haus on 29.11.18.
//  Copyright © 2018 Daniel Haus. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    let gameShortcutItem = UIApplicationShortcutItem(
        type: "Game",
        localizedTitle: "Game",
        localizedSubtitle: "Switch to game device",
        icon: UIApplicationShortcutIcon(type: .home),
        userInfo: nil
    )
    
    let mediaShortcutItem = UIApplicationShortcutItem(
        type: "Media Player",
        localizedTitle: "Media Player",
        localizedSubtitle: "Switch to media player",
        icon: UIApplicationShortcutIcon(type: .play),
        userInfo: nil
    )

    let powerOffShortcutItem = UIApplicationShortcutItem(
        type: "Power",
        localizedTitle: "Power ",
        localizedSubtitle: "Toggle power state",
        icon: nil,
        userInfo: nil
    )
    
    // MARK: -
    // MARK: Shortcuts
    
    func setupShortcuts(_ application: UIApplication) {
        // TODO: add routing switches
        // TODO: maybe mute
        
        application.shortcutItems = [
            gameShortcutItem,
            mediaShortcutItem,
            powerOffShortcutItem
        ]
    }
    
    func handleShortcut(_ shortcutItem: UIApplicationShortcutItem) {
        guard let device = mostRecentlySelectedDevice else {
            return
        }
        
        switch shortcutItem {
        case gameShortcutItem:
            device.change(inputSource: .game)
            
        case mediaShortcutItem:
            device.change(inputSource: .mediaplayer)
            
        case powerOffShortcutItem:
            if let powerState = device.powerState {
                if powerState == .on {
                    device.change(powerState: .standby)
                } else {
                    device.change(powerState: .on)
                }
            }
            
        default:
            return
        }
    }
    
    // MARK: -
    // MARK: App Delegate
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        setupShortcuts(application)
        
        if let shortcutItem = launchOptions?[UIApplication.LaunchOptionsKey.shortcutItem] as? UIApplicationShortcutItem {
            handleShortcut(shortcutItem)
        }
        
        return true
    }
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        handleShortcut(shortcutItem)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

