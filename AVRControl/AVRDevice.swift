//
//  AVRDevice.swift
//  AVRControl
//
//  Created by Daniel Haus on 29.11.18.
//  Copyright © 2018 Daniel Haus. All rights reserved.
//

import UIKit

enum AVRPowerState: String, CaseIterable {
    case standby = "STANDBY"
    case on = "ON"
}

enum AVRInputSource: String, CaseIterable {
    case game = "GAME"
    case mediaplayer = "MPLAY"
    case tv = "TV"
}

protocol AVRDeviceDelegate {
    func didChange(_ sender: AVRDevice, inputSource: AVRInputSource)
    func didChange(_ sender: AVRDevice, powerState: AVRPowerState)
    func didChange(_ sender: AVRDevice, volume: Float)
    func didChange(_ sender: AVRDevice, maxVolume: Float)
    func didChange(_ sender: AVRDevice, mute: Bool)
}

extension AVRDeviceDelegate {
    func didChange(_ sender: AVRDevice, inputSource: AVRInputSource) {}
    func didChange(_ sender: AVRDevice, powerState: AVRPowerState) {}
    func didChange(_ sender: AVRDevice, volume: Float) {}
    func didChange(_ sender: AVRDevice, maxVolume: Float) {}
    func didChange(_ sender: AVRDevice, mute: Bool) {}
}

class AVRDevice: NSObject, AVRConnectionDelegate {
    var name: String
    var host: String
    var port: Int = 23
    var powerState: AVRPowerState?
    var volume: Float?
    var maxVolume: Float?
    var mute: Bool?
    var inputSource: AVRInputSource?
    var connection: AVRDeviceConnection?
    var updateTimer: Timer?
    
    var delegate: AVRDeviceDelegate?
    
    init(name: String, host: String, port: Int = 23) {
        self.name = name
        self.host = host
        self.port = port
    
        super.init()
    }
    
    deinit {
        disconnect()
    }
    
    func connect() {
        disconnect()

        let connection = AVRDeviceConnection(host: host, port: port)
        self.connection = connection
        
        connection.delegate = self
        connection.open()
        connection.queryInitialState()
        
        updateTimer?.invalidate()
        updateTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { [weak self] timer in
            self?.connection?.queryInitialState()
        })
    }
    
    func disconnect() {
        updateTimer?.invalidate()
        connection?.close()
    }
    
    func change(volume: Float) {
        connection?.change(volume: volume)
    }
    
    func change(inputSource: AVRInputSource) {
        connection?.change(inputSource: inputSource)
    }

    func change(powerState: AVRPowerState) {
        connection?.change(powerState: powerState)
    }
    
    // MARK: -
    // MARK: AVR Connection Delegate
    
    func didReceiveVolumeChange(_ sender: AVRDeviceConnection, volume: Float) {
        self.volume = volume
        delegate?.didChange(self, volume: volume)
    }

    func didReceiveMaxVolumeChange(_ sender: AVRDeviceConnection, maxVolume: Float) {
        self.maxVolume = maxVolume
        delegate?.didChange(self, maxVolume: maxVolume)
    }

    func didReceiveMuteChange(_ sender: AVRDeviceConnection, mute: Bool) {
        self.mute = mute
        delegate?.didChange(self, mute: mute)
    }
    
    func didReceivePowerStateChange(_ sender: AVRDeviceConnection, powerState: AVRPowerState) {
        self.powerState = powerState
        delegate?.didChange(self, powerState: powerState)

    }
    
    func didReceiveInputSourceChange(_ sender: AVRDeviceConnection, inputSource: AVRInputSource) {
        self.inputSource = inputSource
        delegate?.didChange(self, inputSource: inputSource)
    }
}
