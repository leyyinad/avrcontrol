//
//  AVRDeviceDiscoverer.swift
//  AVRControl
//
//  Created by Daniel Haus on 21.06.19.
//  Copyright © 2019 Daniel Haus. All rights reserved.
//

import Foundation

protocol AVRDeviceDiscovererDelegate {
    func avrDeviceDiscoverer(_ discoverer: AVRDeviceDiscoverer, didFind deviceName: String, moreComing: Bool)
    func avrDeviceDiscoverer(_ discoverer: AVRDeviceDiscoverer, didRemove deviceName: String, moreComing: Bool)
    func avrDeviceDiscoverer(_ discoverer: AVRDeviceDiscoverer, didResolve address: String, for deviceName: String)
}

class AVRDeviceDiscoverer : NSObject, NetServiceBrowserDelegate, NetServiceDelegate {
    let browser = NetServiceBrowser()
    var searching = false
    var delegate: AVRDeviceDiscovererDelegate?
    var resolving: [String: NetService] = [:]
    
    override init() {
        super.init()
        
        browser.delegate = self
    }
    
    func discover() {
        browser.stop()
        browser.searchForServices(ofType: "_http._tcp.", inDomain: "")
    }
    
    func isAVRDevice(_ service: NetService) -> Bool {
        if service.name.starts(with: "DENON") {
            return true
        }
        return false
    }
    
    // MARK: -
    // MARK: NetServiceBrowser Delegate
    
    func netServiceBrowserWillSearch(_ browser: NetServiceBrowser) {
        searching = true
    }
    
    func netServiceBrowserDidStopSearch(_ browser: NetServiceBrowser) {
        searching = false
    }
    
    func netServiceBrowser(_ browser: NetServiceBrowser, didFind service: NetService, moreComing: Bool) {
        guard isAVRDevice(service) else {
            return
        }
        
        resolving[service.name] = service
        service.delegate = self
        service.resolve(withTimeout: 5)
        
        delegate?.avrDeviceDiscoverer(self, didFind: service.name, moreComing: moreComing)
    }
    
    func netServiceBrowser(_ browser: NetServiceBrowser, didRemove service: NetService, moreComing: Bool) {
        delegate?.avrDeviceDiscoverer(self, didRemove: service.name, moreComing: moreComing)
    }
    
    func netServiceBrowser(_ browser: NetServiceBrowser, didNotSearch errorDict: [String : NSNumber]) {
        self.searching = false
        print(errorDict);
    }
    
    // MARK: -
    // MARK: NetService Delegate
    
    func netServiceDidResolveAddress(_ sender: NetService) {
        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
        guard let data = sender.addresses?.first else { return }
        data.withUnsafeBytes { ptr in
            guard let sockaddr_ptr = ptr.baseAddress?.assumingMemoryBound(to: sockaddr.self) else {
                // handle error
                return
            }
            let sockaddr = sockaddr_ptr.pointee
            guard getnameinfo(sockaddr_ptr, socklen_t(sockaddr.sa_len), &hostname, socklen_t(hostname.count), nil, 0, NI_NUMERICHOST) == 0 else {
                return
            }
        }
        let ipAddress = String(cString:hostname)
        resolving.removeValue(forKey: sender.name)
        delegate?.avrDeviceDiscoverer(self, didResolve: ipAddress, for: sender.name)
    }
}
