//
//  ViewController.swift
//  AVRControl
//
//  Created by Daniel Haus on 29.11.18.
//  Copyright © 2018 Daniel Haus. All rights reserved.
//

import UIKit

let host = "denon-avr-2113.local"
let port = 23

class ViewController: UIViewController, AVRDeviceDelegate {

    @IBOutlet weak var deviceDisplay: UIButton!
    @IBOutlet weak var volumeDisplay: UILabel!
    @IBOutlet weak var powerIndicator: UIView!
    @IBOutlet weak var volumeKnob: Knob!
    @IBOutlet weak var sourceMediaIndicator: UIView!
    @IBOutlet weak var sourceGameIndicator: UIView!
    @IBOutlet weak var sourceTVIndicator: UIView!

    var currentDevice: AVRDevice?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        volumeKnob.minimumValue = 0
        volumeKnob.maximumValue = 60
        volumeKnob.lineWidth = 4
        volumeKnob.pointerLength = 12

        powerIndicator.isHidden = true
        
        deviceDisplay.setTitle(currentDevice?.name ?? "No device", for: .normal)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        currentDevice?.disconnect()
        currentDevice = nil
    }
    
    func useDevice(_ device: AVRDevice) {
        guard device != currentDevice else {
            return
        }
        
        currentDevice?.disconnect()
        currentDevice?.delegate = nil
        
        currentDevice = device
        device.delegate = self
        device.connect()
    }
    
    func update(inputSource: AVRInputSource) {
        sourceMediaIndicator.isHidden = inputSource != .mediaplayer
        sourceGameIndicator.isHidden = inputSource != .game
        sourceTVIndicator.isHidden = inputSource != .tv
    }
    
    func update(powerState: AVRPowerState) {
        powerIndicator.isHidden = powerState != .on
    }

    func update(volume: Float) {
        volumeDisplay.text = String(format: "%02.0f", volume)
        volumeKnob.setValue(volume)
    }
    
    func update(maxVolume: Float) {
        if volumeKnob.maximumValue != maxVolume {
            volumeKnob.maximumValue = maxVolume
        }
    }
    
    // MARK: -
    // MARK: Actions
    
    @IBAction func changeVolume(_ sender: Knob) {
        let volume = sender.value
        update(volume: volume)
        currentDevice?.change(volume: volume)
    }

    @IBAction func switchToAppleTV(_ sender: Any) {
        update(inputSource: .mediaplayer)
        currentDevice?.change(inputSource: .mediaplayer)
    }
    
    @IBAction func switchToPS(_ sender: Any) {
        update(inputSource: .game)
        currentDevice?.change(inputSource: .game)
    }
    
    @IBAction func switchToTV(_ sender: Any) {
        update(inputSource: .tv)
        currentDevice?.change(inputSource: .tv)
    }

    @IBAction func toggleStandby(_ sender: Any) {
        var state: AVRPowerState = .on
        if currentDevice?.powerState == .on {
            state = .standby
        }
        update(powerState: state)
        currentDevice?.change(powerState: state)
    }

    // MARK: -
    // MARK: AVR Device Delegate
    
    func didChange(_ sender: AVRDevice, inputSource: AVRInputSource) {
        update(inputSource: inputSource)
    }
    
    func didChange(_ sender: AVRDevice, powerState: AVRPowerState) {
        update(powerState: powerState)
    }
    
    func didChange(_ sender: AVRDevice, volume: Float) {
        update(volume: volume)
    }
    
    func didChange(_ sender: AVRDevice, maxVolume: Float) {
        update(maxVolume: maxVolume)
    }
    
    @IBAction func deviceNameTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
