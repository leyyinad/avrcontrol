//
//  AVRDeviceConnection.swift
//  AVRControl
//
//  Created by Daniel Haus on 30.11.18.
//  Copyright © 2018 Daniel Haus. All rights reserved.
//

import UIKit

let maxTransmissionLength = 140
let verbose = false
let defaultAutoCloseTimeout: TimeInterval = 1.0

protocol AVRConnectionDelegate {
    func didReceiveVolumeChange(_ sender: AVRDeviceConnection,
                                volume: Float)

    func didReceiveMaxVolumeChange(_ sender: AVRDeviceConnection,
                                maxVolume: Float)

    func didReceiveInputSourceChange(_ sender: AVRDeviceConnection,
                                     inputSource: AVRInputSource)

    func didReceivePowerStateChange(_ sender: AVRDeviceConnection,
                                    powerState: AVRPowerState)

    func didReceiveMuteChange(_ sender: AVRDeviceConnection,
                              muted: Bool)
}

extension AVRConnectionDelegate {
    func didReceiveVolumeChange(_ sender: AVRDeviceConnection,
                                volume: Float) { }

    func didReceiveMaxVolumeChange(_ sender: AVRDeviceConnection,
                                   maxVolume: Float) { }

    func didReceiveInputSourceChange(_ sender: AVRDeviceConnection,
                                     inputSource: AVRInputSource) { }

    func didReceivePowerStateChange(_ sender: AVRDeviceConnection,
                                    powerState: AVRPowerState) { }

    func didReceiveMuteChange(_ sender: AVRDeviceConnection,
                              muted: Bool) { }
}

class AVRDeviceConnection: NSObject, StreamDelegate {
    var host: String
    var port: Int = 23
    var autoCloseTimeout: TimeInterval = defaultAutoCloseTimeout

    var inputStream: InputStream?
    var outputStream: OutputStream?

    var closeConnectionTime: Date?
    var autoCloseTimer: Timer?
    
    var delegate: AVRConnectionDelegate?

    init(host: String, port: Int = 23, autoCloseTimeout: TimeInterval = defaultAutoCloseTimeout) {
        self.host = host
        self.port = port
        self.autoCloseTimeout = autoCloseTimeout
    }

    deinit {
        close()
    }

    // MARK: -
    // MARK: High Level Interface
    
    func queryInitialState() {
        send("PW?")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.send("SI?")
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.send("MU?")
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.send("MV?")
        }
    }

    func change(inputSource: AVRInputSource) {
        send("SI\(inputSource.rawValue)")
    }

    func change(powerState: AVRPowerState) {
        send("PW\(powerState.rawValue)")
    }

    func change(volume: Float) {
        let vol = volume.rounded()
        send("MV\(String(format: "%02.0f", vol))")
    }

    func mute() {
        send("MUON")
    }

    func unmute() {
        send("MUOFF")
    }

    // MARK: -
    // MARK: Connection Handling
    
    func open() {
        Stream.getStreamsToHost(withName: host, port: port, inputStream: &inputStream, outputStream: &outputStream)
        
        if let inputStream = inputStream {
            inputStream.delegate = self
            
            inputStream.schedule(in: RunLoop.current,
                                 forMode: RunLoop.Mode.default)
            inputStream.open()
        } else {
            print("No input stream")
        }
        
        if let outputStream = outputStream {
            outputStream.open()
            updateCloseConnectionTime()
            autoCloseTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: {_ in
                guard self.autoCloseTimeout > 0.0 else {
                    return
                }
                
                if let cct = self.closeConnectionTime, cct < Date() {
                    self.close()
                }
            })
        } else {
            print("No output stream")
        }
    }
    
    func close() {
        autoCloseTimer?.invalidate()
        autoCloseTimer = nil

        inputStream?.close()
        outputStream?.close()
    }
    
    func updateCloseConnectionTime() {
        if autoCloseTimeout > 0 {
            closeConnectionTime = Date(timeIntervalSinceNow: autoCloseTimeout)
        }
    }
    
    func reopen() {
        close()
        open()
    }
    
    // MARK: -
    // MARK: Transmission
    
    func send(_ command: String) {
        if verbose {
            print("send: \(command)")
        }
        
        let seq = "\(command)\r"
        if let outputStream = outputStream {
            if outputStream.hasSpaceAvailable {
                outputStream.write(seq, maxLength: seq.count)
            } else {
                print("Can't write to output stream")
                reopen()
            }
        } else {
            print("Not output stream")
        }
        
        updateCloseConnectionTime()
    }
    
    func receive(_ response: String) {
        let index = String.Index(utf16Offset: 2, in: response)
        let command = response[..<index]
        let parameter = response[index...]

        switch command {
        case "MV":
            receiveVolumeChange(parameter)
        case "MU":
            receiveMuteChange(parameter)
        case "PW":
            receivePowerStateChange(parameter)
        case "SI":
            receiveInputSourceChange(parameter)
        default:
            if verbose {
                print("Unhandled command: \(command)\(parameter)")
            }
        }

        updateCloseConnectionTime()
    }
    
    func receiveVolumeChange(_ parameter: Substring) {
        if parameter.starts(with: "MAX") {
            let s = parameter.split(separator: " ")[1]
            let vol = parse(volumeString: s)
            delegate?.didReceiveMaxVolumeChange(self, maxVolume: vol)
        } else {
            let vol = parse(volumeString: parameter)
            delegate?.didReceiveVolumeChange(self, volume: vol)
        }
    }
    
    func receiveMuteChange(_ parameter: Substring) {
        if parameter == "ON" {
            delegate?.didReceiveMuteChange(self, muted: true)
        } else if parameter == "OFF" {
            delegate?.didReceiveMuteChange(self, muted: false)
        }
    }
    
    func receivePowerStateChange(_ parameter: Substring) {
        for powerState in AVRPowerState.allCases {
            if powerState.rawValue == parameter {
                delegate?.didReceivePowerStateChange(self, powerState: powerState)
                break
            }
        }
    }
    
    func receiveInputSourceChange(_ parameter: Substring) {
        for inputSource in AVRInputSource.allCases {
            if inputSource.rawValue == parameter {
                delegate?.didReceiveInputSourceChange(self, inputSource: inputSource)
                break
            }
        }
    }

    // MARK: -
    // MARK: Stream Delegate

    func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
        if eventCode == Stream.Event.hasBytesAvailable,
            let inputStream = aStream as? InputStream {
            let maxLength = 140
            var buffer = [UInt8](repeating: 0, count: maxLength)

            inputStream.read(&buffer, maxLength: maxLength)

            let bufferStr = String(bytes: buffer, encoding: String.Encoding.utf8)
            let response = bufferStr?.split(separator: "\r",
                                            maxSplits: 2,
                                            omittingEmptySubsequences: true)[0]

            receive(String(response!))
        }
    }
    
    // MARK: -
    // MARK: Helpers
    
    func parse(volumeString: Substring) -> Float {
        let s = volumeString.trimmingCharacters(in: .whitespacesAndNewlines)
        var volume = Float(s)!
        
        if s.count == 3 {
            volume = 0.1 * volume
        }
        
        return volume
    }
}
