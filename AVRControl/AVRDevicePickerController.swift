//
//  AVRDevicePickerController.swift
//  AVRControl
//
//  Created by Daniel Haus on 22.06.19.
//  Copyright © 2019 Daniel Haus. All rights reserved.
//

import UIKit


var mostRecentlySelectedDevice: AVRDevice? = nil

class AVRDevicePickerController : UITableViewController, AVRDeviceDiscovererDelegate {
    let discoverer = AVRDeviceDiscoverer()
    var addresses: [String:String] = [:]
    var deviceOrder: [String] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        discoverer.delegate = self
        discoverer.discover()
    }
    
    func indexPathFor(_ deviceName: String) -> IndexPath? {
        if let index = deviceOrder.firstIndex(of: deviceName) {
            return IndexPath(row: index, section: 0)
        }
        return nil
    }

    
    // MARK: -
    // MARK: Table View Data Source

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AVRDeviceCell", for: indexPath)
        let deviceName = deviceOrder[indexPath.row]
        let address = addresses[deviceName]
        
        cell.textLabel?.text = deviceName
        cell.textLabel?.isEnabled = address != nil
        cell.detailTextLabel?.text = address
        
        return cell
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deviceOrder.count
    }
    
    
    // MARK: -
    // MARK: AVR Device Discoverer Delegate
    
    func avrDeviceDiscoverer(_ discoverer: AVRDeviceDiscoverer, didFind deviceName: String, moreComing: Bool) {
        if deviceOrder.firstIndex(of: deviceName) == nil {
            deviceOrder.append(deviceName)
            deviceOrder.sort()
            
            if let indexPath = indexPathFor(deviceName) {
                tableView.insertRows(at: [indexPath], with: .automatic)
            }
        } else if let indexPath = indexPathFor(deviceName) {
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    func avrDeviceDiscoverer(_ discoverer: AVRDeviceDiscoverer, didRemove deviceName: String, moreComing: Bool) {
        guard let indexPath = indexPathFor(deviceName) else {
            return
        }
        
        tableView.deleteRows(at: [indexPath], with: .automatic)
        deviceOrder.remove(at: indexPath.row)
        // TODO: what happens if not resolved?
        addresses.removeValue(forKey: deviceName)
    }
    
    func avrDeviceDiscoverer(_ discoverer: AVRDeviceDiscoverer, didResolve address: String, for deviceName: String) {
        guard let indexPath = indexPathFor(deviceName) else {
            return
        }

        addresses[deviceName] = address
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }

    
    // MARK: -
    // MARK: Storyboard Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ViewController,
            let cell = sender as? UITableViewCell,
            let indexPath = tableView.indexPath(for: cell),
            let address = addresses[deviceOrder[indexPath.row]] {
            let deviceName = deviceOrder[indexPath.row]
            let device = AVRDevice(name: deviceName, host: address)
            destination.useDevice(device)
            mostRecentlySelectedDevice = device
        }
    }
}
